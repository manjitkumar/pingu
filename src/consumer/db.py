import psycopg2

import common

logger = common.logger.get_logger(__name__)
INSERT_METRICS_RAW_SQL = (
    """INSERT INTO metrics (url, status_code, regex_pattern, is_regex_match,
    response_time, ping_timestamp, error, error_description)
    VALUES (%(url)s, %(status_code)s, %(regex_pattern)s, %(is_regex_match)s,
    %(response_time)s, %(ping_timestamp)s, %(error)s, %(error_description)s);"""
)


class Database:
    def __init__(self, autocommit=True):
        config = common.config.load_config()
        db_config = config['postgres']
        self.client = psycopg2.connect(
            host=db_config['host'],
            dbname=db_config['db'],
            user=db_config['username'],
            password=db_config['password'],
            port=db_config['port'],
            sslmode=db_config['sslmode']
        )
        self.client.autocommit = autocommit
        self.raw_sql = INSERT_METRICS_RAW_SQL

    def insert_rows(self, rows):
        cursor = self.client.cursor()

        for row_data in rows:
            logger.debug(f'execute sql: {self.raw_sql}, {row_data}')
            cursor.execute(self.raw_sql, row_data)
