import json
from kafka import KafkaConsumer

import common
from consumer.db import Database

config = common.config.load_config()
logger = common.logger.get_logger(__name__)


class KafkaReader:
    def __init__(self,):
        self.config = config['kafka']
        self.kafka = KafkaConsumer(
            self.config['topic'],
            security_protocol='SSL',
            auto_offset_reset='earliest',
            consumer_timeout_ms=1000,
            bootstrap_servers=self.config['host'],
            ssl_cafile=self.config['ssl_cafile'],
            ssl_certfile=self.config['ssl_certfile'],
            ssl_keyfile=self.config['ssl_keyfile'],
        )
        self.db = Database()

    def fetch_messages(self, timeout):
        messages = []
        for message in self.kafka:
            messages.append(json.loads(message.value.decode('utf-8')))
        return messages

    def save_messages(self, messages):
        self.db.insert_rows(messages)
