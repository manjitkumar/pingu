from time import sleep
from kafka.errors import KafkaTimeoutError, NoBrokersAvailable

import common
from consumer.reader import KafkaReader


reader = KafkaReader()
config = common.config.load_config()
logger = common.logger.get_logger(__name__)


def process_messages():
    try:
        messages = reader.fetch_messages(30 * 1000)  # timeout 30s
        logger.info(f'#{len(messages)} fetched from kafka')
        reader.save_messages(messages)
    except (NoBrokersAvailable, KafkaTimeoutError):
        logger.error(f'KafkaError occuered while fetching messages', exc_info=True)


if __name__ == '__main__':
    try:
        while(True):
            process_messages()
            sleep(config['consumer']['interval'])
    except (KeyboardInterrupt, SystemExit):
        pass
