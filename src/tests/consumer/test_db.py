from unittest import mock

from consumer.db import Database


@mock.patch('common.config.load_config')
def test_db_database_init(load_config_mock, initialise_database, test_config):
    # given
    load_config_mock.return_value = test_config

    # when
    db = Database()

    # then
    assert db.client.autocommit is True
    assert db.client.info.dbname == 'pingu_db'
    assert db.client.info.host == 'postgresql'
    assert db.client.info.port == 5432
    load_config_mock.assert_called_once()


@mock.patch('common.config.load_config')
def test_db_insert_rows(load_config_mock, initialise_database, test_config, kafka_response):
    # given
    load_config_mock.return_value = test_config
    rows = kafka_response
    db = Database()

    # when
    db.insert_rows(rows)

    # then
    cursor = db.client.cursor()
    cursor.execute('SELECT * FROM metrics')
    records = cursor.fetchall()
    assert len(records) == 3
    load_config_mock.assert_called_once()
