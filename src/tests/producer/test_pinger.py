import datetime
from unittest import mock

import requests
from producer.pinger import Pinger


@mock.patch('requests.get')
def test_pinger_ping_ok(requests_get_mock):
    # given
    pinger = Pinger()
    website = {
        'name': 'Manjit',
        'url': 'manjitkumar.github.io',
        'regex_pattern': 'source'
    }
    requests_get_mock.return_value.status_code = 200
    requests_get_mock.return_value.text = 'Html page source code'
    requests_get_mock.return_value.elapsed = datetime.timedelta(seconds=1)

    # when
    response = pinger.ping(website)

    # then
    assert response['status_code'] == 200
    assert response['url'] == 'manjitkumar.github.io'
    assert response['regex_pattern'] == 'source'
    assert response['response_time'] == 1.0 * 1000
    assert response['is_regex_match'] is True
    assert response['error'] is None
    assert response['error_description'] is None
    requests_get_mock.assert_called_once()


@mock.patch('requests.get')
def test_pinger_ping_error(requests_get_mock):
    # given
    pinger = Pinger()
    website = {
        'name': 'Manjit',
        'url': 'manjitkumar.github.io',
        'regex_pattern': 'source'
    }
    expected_error = requests.exceptions.ConnectionError()
    requests_get_mock.side_effect = expected_error

    # when
    response = pinger.ping(website)

    # then
    assert response['status_code'] is None
    assert response['url'] == 'manjitkumar.github.io'
    assert response['regex_pattern'] == 'source'
    assert response['response_time'] is None
    assert response['is_regex_match'] is None
    assert response['error'] == 'RequestException'
    assert response['error_description'] == str(expected_error)
    requests_get_mock.assert_called_once()
