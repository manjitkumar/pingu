import time
import yaml
import pytest
import psycopg2

from common import logger


logger = logger.get_logger(__name__)
CONFIG_FILE = '/configs/tests/config.yaml'


def get_pg_client_connection():
    config = yaml.safe_load(open(CONFIG_FILE))['postgres']
    try:
        pg_client = psycopg2.connect(
            host=config['host'],
            dbname=config['db'],
            user=config['username'],
            password=config['password'],
            port=config['port'],
        )
    except psycopg2.OperationalError:
        logger.warning('OperationalError: While conneecting to postgresql')
        logger.warning('Reconnecting in 15 seconds...')
        time.sleep(15)  # require about 15 seconds for postgresql container to accept connections
        pg_client = psycopg2.connect(
            host=config['host'],
            dbname=config['db'],
            user=config['username'],
            password=config['password'],
            port=config['port'],
        )
    pg_client.autocommit = True
    return pg_client


@pytest.fixture(scope='module')
def initialise_database():
    pg_client = get_pg_client_connection()
    try:
        cursor = pg_client.cursor()
        cursor.execute('DELETE FROM metrics;')
    except psycopg2.errors.UndefinedTable:
        cursor.execute(open('/configs/consumer/metrics.sql', 'r').read())
        cursor.execute('SELECT * FROM metrics LIMIT 1;')
    except psycopg2.errors.DuplicateTable:
        pass
    except psycopg2.errors.UndefinedTable:
        raise


@pytest.fixture(scope='module')
def test_config():
    with open(CONFIG_FILE) as stream:
        try:
            loaded_config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            raise exc
    return loaded_config


@pytest.fixture(scope='function')
def kafka_response():
    return [
        {
            'url': 'https://saurabhjha.github.io',
            'status_code': 200,
            'ping_timestamp': '2020-10-11 20:36:43.574592',
            'response_time': 396.95399999999995,
            'regex_pattern': 'r"[0-9]"',
            'is_regex_match': False,
            'error': None,
            'error_description': None
        }, {
            'url': 'https://manjitkumar.github.io',
            'status_code': 200,
            'ping_timestamp': '2020-10-11 20:36:43.986709',
            'response_time': 260.53700000000003,
            'regex_pattern': 'r"[A-Za-z]"',
            'is_regex_match': False,
            'error': None,
            'error_description': None
        }, {
            'url': 'https://random.github.ios',
            'status_code': None,
            'ping_timestamp': '2020-10-11 20:36:44.262835',
            'response_time': None,
            'regex_pattern': 'random',
            'is_regex_match': None,
            'error': 'RequestException',
            'error_description': (
                "HTTPSConnectionPool(host='random.github.ios', port=443): Max retries exceeded "
                "with url: / (Caused by NewConnectionError('<urllib3.connection.HTTPSConnection "
                'object at 0x7f9c788e9e48>: Failed to establish a new connection: [Errno -2] Name '
                "or service not known',))"
            )
        }
    ]
