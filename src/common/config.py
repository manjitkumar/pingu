import yaml


def load_config():
    with open('/configs/common/config.yaml') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            raise exc
