from time import sleep
from kafka.errors import KafkaTimeoutError, NoBrokersAvailable


import common
from producer.checks import load_checks
from producer.pinger import Pinger
from producer.publisher import KafkaPublisher


pinger = Pinger()
publisher = KafkaPublisher()
checks = load_checks()
config = common.config.load_config()
logger = common.logger.get_logger(__name__)


def process_checks():
    websites = checks['websites']
    for website in websites:
        try:
            response = pinger.ping(website)
            publisher.publish(response)
        except (NoBrokersAvailable, KafkaTimeoutError):
            logger.error(f'KafkaError occuered while pinging {website["url"]}', exc_info=True)


if __name__ == '__main__':
    try:
        while(True):
            process_checks()
            sleep(config['producer']['interval'])
    except (KeyboardInterrupt, SystemExit):
        pass
