import yaml


def load_checks():
    with open('/configs/producer/checks.yaml') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            raise exc
