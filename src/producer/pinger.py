import re
import requests
from datetime import datetime

from common import logger
from common import constants


logger = logger.get_logger(__name__)


class Pinger:
    def ping(self, website):
        url = website['url']
        logger.info(f'pinging url: {url}')
        regex_pattern = website['regex_pattern']
        timestamp = datetime.utcnow().strftime(constants.DATETIME_FORMAT)

        try:
            response = requests.get(url)
            logger.info(f'pinged url: {url}; status_code: {response.status_code}')

            response_data = {
                'url': url,
                'status_code': response.status_code,
                'ping_timestamp': timestamp,
                'response_time': response.elapsed.total_seconds() * 1000,
                'regex_pattern': regex_pattern,
                'is_regex_match': self._is_regex_pattern_match(regex_pattern, response.text),
                'error': None,
                'error_description': None,
            }
        except requests.exceptions.RequestException as err:
            response_data = {
                'url': url,
                'status_code': None,
                'ping_timestamp': timestamp,
                'response_time': None,
                'regex_pattern': regex_pattern,
                'is_regex_match': None,
                'error': 'RequestException',
                'error_description': str(err),
            }

        logger.debug(f'response_data: {response_data}')
        return response_data

    def _is_regex_pattern_match(self, regex_pattern, content):
        if regex_pattern:
            return bool(re.search(regex_pattern, content))
