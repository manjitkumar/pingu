import json
from kafka import KafkaProducer

import common
config = common.config.load_config()
logger = common.logger.get_logger(__name__)


class KafkaPublisher:
    def __init__(self,):
        self.config = config['kafka']
        self.kafka = KafkaProducer(
            security_protocol='SSL',
            bootstrap_servers=self.config['host'],
            ssl_cafile=self.config['ssl_cafile'],
            ssl_certfile=self.config['ssl_certfile'],
            ssl_keyfile=self.config['ssl_keyfile'],
        )

    def publish(self, data):
        logger.info(f'publishing data to kafka for url: {data["url"]}')
        self.kafka.send(self.config['topic'], json.dumps(data).encode('utf-8'))
        logger.info(f'published data to kafka for url: {data["url"]}')
