# Pingu
Pingu is a python application that mainly consists of two components named as [`producer`](https://gitlab.com/manjitkumar/pingu/-/tree/main/src/producer) and [`consumer`](https://gitlab.com/manjitkumar/pingu/-/tree/main/src/consumer). The producer component is responsible for pinging a set of websites configured in the configuration file at a fixed interval and publish ping metrics to a Kafka topic that is further consumed by the consumer component and then saved into a PostgreSQL database table for long term persistence.

## Requirements
### Docker 2.4.0.0
  * You need to have [Docker](https://www.docker.com/) installed and running on your machine.

### PostgreSQL DB
* You need to have a Postgres instance running with a DB `pingu_db`
* Run [metrics.sql](https://gitlab.com/manjitkumar/pingu/blob/main/configs/consumer/metrics.sql) to create `metrics` table to persist ping metrics
* Credentials can be found/updated in [configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs)

### Kafka Cluster
* You need to have a Kafka cluster running with a topic `pings`
* Credentials can be found/updated in [configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs)

## Configurations
Configurations for the applications can be found in [configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs) directory as
 - [test configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs/tests)
 - [common configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs/common)
 - [consumer configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs/consumer)
 - [producer configs](https://gitlab.com/manjitkumar/pingu/-/tree/main/configs/producer)


## Steps to Run Tests
1. Clone this repository to your machine using the command `git clone git@gitlab.com:manjitkumar/pingu.git`
2. Step into the cloned repository using the command `cd pingu`
3. Once inside the project directory, run command `docker-compose run --rm test` to run tests
   - **Note:** First attempt to run tests might fail as the PostgreSQL container may take a bit longer to be online to accept connections than the pytests container.
   - **Quick Fix:** Retrying command `docker-compose run --rm test` should work as PostgreSQL should be ready to accept connections.
   - **Long Term Fix:** A [wait-for-it](https://docs.docker.com/compose/startup-order/) script can be used to solve this issue.


## Steps to Run Pingu
1. Clone this repository to your machine using the command `git clone git@gitlab.com:manjitkumar/pingu.git`
2. Step into the cloned repository using the command `cd pingu`
3. Once inside the project directory, run command `docker-compose up --build` to start the pingu application
   * run command `docker-compose run --rm producer` to run only producer component
   * run command `docker-compose run --rm consumer` to run only consumer component
4. Application logs can be found in pingu/logs/ directory

# Limitations / Trade-Offs
1. Currently Pingu is synchronous and makes requests to configured websites in sync.
2. There is no support for initial DB migrations and Kafka topic setup.
3. Offset reads aren't handled for consumer restarts in this implementation yet.
4. Proper use of Kafka partitioning and python's async io can be proven beneficial to the overall performance of Pingu.
