
-- Table Definition ----------------------------------------------

CREATE TABLE metrics (
    id SERIAL PRIMARY KEY,
    url character varying(512) NOT NULL,
    status_code integer NULL,
    regex_pattern character varying(255) NULL,
    is_regex_match boolean NULL,
    response_time bigint NULL,
    ping_timestamp timestamp with time zone NOT NULL,
    error character varying(64) NULL,
    error_description character varying(512) NULL,
    created_at timestamp with time zone default (now() at time zone 'utc')
);


-- Indices -------------------------------------------------------

CREATE INDEX metric_status_code_560c9c85 ON metrics(status_code int4_ops);
